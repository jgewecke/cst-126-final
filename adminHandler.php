<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp
// --NOTE--
// Post title is <=100 characters
// Post text is <= 2000 characters
// A chat filter is also added for some inappropriate words

require_once('myfuncs.php');
$link = dbConnect();
    
$people = $_POST['person'];


foreach($people as $person)
{
    $id = $person["ID"];
    $firstname = $person["First Name"];
    $lastname = $person["Last Name"];
    $email = $person["Email"];
    $permissionLevel = $person["Permission Level"];

    // Check for empty input, otherwise exit out with an error
    if ($firstname == NULL) { $message = "The First Name or is a required field and cannot be blank.\n"; gotoResultPage($message); }
    if ($lastname == NULL)  { $message = "The Last Name is a required field and cannot be blank.\n"; gotoResultPage($message); }
    if ($email == NULL)     { $message = "The email is a required field and cannot be blank.\n"; gotoResultPage($message); }
    if ($permissionLevel == NULL)  { $message = "The permission level is a required field and cannot be blank.\n"; gotoResultPage($message); }

    // Check length
    if ($permissionLevel > 2)   gotoResultPage("Cannot give permission higher than 2");
    if ($permissionLevel < 0)   gotoResultPage("Cannot give permission lower than 0");

    // Check for illegal characters
    $illegal = array("'", "\"", "/", "\\", "[", "]", "{", "}", "(", ")");
    foreach ($illegal as $i) {
        if (strpos($firstname, $i) !== false) {
            gotoResultPage("ERROR: The username has one of the illegal characters: ' \" / \ [ ] ( ) { }\n");
        }
    }

    foreach ($illegal as $i) {
        if (strpos($lastname, $i) !== false) {
            gotoResultPage("ERROR: The password has one of the illegal characters: ' \" / \ [ ] ( ) { }\n");
        }
    }

    $sql = "UPDATE users SET FIRST_NAME='$firstname', LAST_NAME='$lastname', EMAIL='$email', PERMISSION_LEVEL='$permissionLevel' WHERE ID='$id'";
    if (!$result = mysqli_query($link, $sql))
    {
        gotoResultPage("ERROR: %s\n" . mysqli_error($link));
    }
}
gotoResultPage('Succesfully updated permissions and information!');

mysqli_close($link);
?>