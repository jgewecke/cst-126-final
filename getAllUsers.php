<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp

// Connect
$link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$sql = "SELECT FIRST_NAME, LAST_NAME FROM users";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        echo $row["FIRST_NAME"]. " " . $row["LAST_NAME"]. "<br>";
    }
}
else {
    echo "0 results";
}


// Close connection
mysqli_close($link);
?>