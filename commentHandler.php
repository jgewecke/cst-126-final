<a href="viewPosts.php">Return to posts.</a>

<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp

    require_once('myfuncs.php');
    $link = dbConnect();
    
    $text = $_POST["comment-area"];
    $post_id = $_POST["POST_ID"]; 

    // Check for valid input /////////////////////////////////////////////////////////////
    // Simple filter
    $bannedWords = ['ass', 'fuck', 'bitch', 'cock', 'cum', 'cunt', 'dumbass', 'fag', 'faggot', 'goddamnit', 'goddamn', 'shit'];
    
    $approved = true;
    $textSizeLimit = 2000; // Number of characters allowed
            
    // Check for bad words
    foreach ($bannedWords as $badWord) {
        if (strpos(strtolower($text), $badWord) !== false) {
            gotoResultPage("The word '" . $badWord . "' cannot be used in your text. Please remove it and try again. ");
        }
    }
    
    // Check for empty fields
    if ($text == NULL)  { gotoResultPage("The text field cannot be blank.\n"); }
    if ($text == "Leave a comment.") { gotoResultPage("Your response cannot be the default.\n"); }
    
    // Check for appropriate lengths
    if (strlen($text) > $textSizeLimit) {
        gotoResultPage("Your blog post is too big. Please make it 2000 characters or less");
    }
    
    $current_user = getUserArrayFromCurrentUser($link);

    $sql = "INSERT INTO comments (POST_ID, USER_ID, FIRST_NAME, LAST_NAME, TEXT) VALUES ('$post_id','".$current_user['ID']."', '".$current_user['FIRST_NAME']."', '".$current_user['LAST_NAME']."', '$text')";
    mysqli_query($link, $sql);

    gotoResultPage("Comment was submitted.");
?>