<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp
require_once('./myfuncs.php');
?>

<!doctype html>
<html>
    <head>
      <title>Final Project</title>
      <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="header">
            <h1>CST-126 Justin Gewecke</h1>
        </div>

        <div class="topnav">
            <a href="index.php">Main Menu</a>
            <a href="viewPosts.php">View All Posts</a>
			<?php require_once('myfuncs.php'); showAdditionalMenus(); ?>
        </div>

        <div class="row">
            <div class="leftcolumn">
                <div class="card">
                    <?php loadModPanel(); ?>
                </div>
            </div>

            <div class="rightcolumn">
                <div class="card">
                    <h2>About Me</h2>
                    <p>Hello, my name is Justin. Thank you for visiting my site!</p>
                </div>
                <div class="card">
                    <h2>Search</h2>
                    <?php include('./search.php') ?>
                </div>
            </div>
        </div>

        <div class="footer">
            <p>Website layout from <a href="https://www.w3schools.com/css/css_website_layout.asp">w3schools.com</a></p> 
        </div>

    </body>
</html>
