<!-- 
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp
-->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Search Menu</title>
	</head>
	<body>
        <form action="searchHandler.php" method="POST">
			<div class="search element">
				<input type="text" name="Search"/>
			</div>
			<div class="search element">
				<input type="submit" name="SearchTitle" value="Search by Title" />
				<input type="submit" name="SearchBody" value="Search within Post" />
			</div>
	</body>
</html>

<?php

require_once './myfuncs.php';

// Connect to db
$link = dbConnect();

$sql = "SELECT TAG FROM posts";

$result = mysqli_query($link, $sql);

$row = $result->fetch_assoc();	// Read the Row from the Query

$array = array();
foreach($result as $i)
{
	if (in_array($i["TAG"], $array))
	{
		continue;
	}
	else
	{
		array_push($array, $i["TAG"]);
		echo('<input type="checkbox" checked name="Tag[]" value="'.$i["TAG"].'">
		<label for="'.$i["TAG"].'"> '.$i["TAG"].'</label><br>
 		');
	}

}
?>