<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp
require_once('myfuncs.php');

$link = dbConnect();

$rating = 0;
$post_id = 0;
$poster_id = 0;
if (isset($_POST['Upvote'])) $rating = $_POST['Upvote'];
if (isset($_POST['Downvote'])) $rating = $_POST['Downvote'];
if (isset($_POST['POST_ID'])) $post_id = $_POST['POST_ID'];
if (isset($_POST['POSTER_ID'])) $poster_id = $_POST['POSTER_ID'];

// We didn't find our elements
if ($rating == 0 || $post_id == 0)
{
    die("Something went wrong in ratingHandler.php");
}
else if ($poster_id == '')
{
    echo('<a href="viewPosts.php">View all posts.</a>
    <a href="search.php">Search for a post.</a>
    <a href="index.php">Return to main menu.</a>');
    die("You must be logged in to do this.");
}

// Check if the user already rated a post
$sql = "SELECT * FROM ratings WHERE USER_ID='$poster_id' AND POST_ID='$post_id'";
$result = mysqli_query($link, $sql);
$numRows = mysqli_num_rows($result);
$exit = false;
if ($numRows >= 1)
{
    $rows = $result->fetch_assoc();	// Read the Row from the Query)
    // They already voted for this
    if ($rows["RATING"] === $rating)
    {
        gotoResultPage("You already rated the post this way.");
    }

    // They want to update their rating
    if ($rows["RATING"] !== $rating)
    {
        $sql = "UPDATE ratings SET RATING='$rating' WHERE USER_ID='$poster_id'";
        mysqli_query($link, $sql);
        gotoResultPage("Updated your rating for this post.");
    }
}
// They didn't rate this post before
else
{
    // Attempt insert
    $sql = "INSERT INTO ratings (USER_ID, POST_ID, RATING) VALUES ('$poster_id', '$post_id', '$rating')";
    if(mysqli_query($link, $sql)){
        gotoResultPage("Your rating was recorded.");
    } else{
        $message = "ERROR: Could not able to execute $sql. " . mysqli_error($link);
        gotoResultPage($message);
    }
}
?>