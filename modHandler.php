<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp

    require_once('myfuncs.php');
    $link = dbConnect();
    
    $postID = $_POST["Data"];
    $title = $_POST["Title"]; 
    $text = $_POST["Text"];
    $tag = $_POST["Tag"];

    // Use this code if submit was pressed
    if (isset($_POST["Submit"]))
    {
        // Check for valid input /////////////////////////////////////////////////////////////
        // Simple filter
        $bannedWords = ['ass', 'fuck', 'bitch', 'cock', 'cum', 'cunt', 'dumbass', 'fag', 'faggot', 'goddamnit', 'goddamn', 'shit'];
        
        $approved = true;
        $titleSizeLimit = 100; // Number of characters allowed
        $textSizeLimit = 2000; // Number of characters allowed
        $tagSizeLimit = 15;    // Number of characters allowed
               
        // Check for bad words
        foreach ($bannedWords as $badWord) {
            if (strpos(strtolower($text), $badWord) !== false) {
                gotoResultPage("The word '" . $badWord . "' cannot be used in your text. Please remove it and try again. ");
            }
            
            if (strpos(strtolower($title), $badWord) !== false) {
                gotoResultPage("The word '" . $badWord . "' cannot be used in your title. Please remove it and try again. ");
            }

            if (strpos(strtolower($tag), $badWord) !== false) {
                gotoResultPage("The word '" . $badWord . "' cannot be used in your title. Please remove it and try again. ");
            }
        }
        
        // Check for empty fields
        if ($text == NULL)  { gotoResultPage("The text field cannot be blank.\n"); }
        if ($title == NULL)  { gotoResultPage("The title cannot be blank.\n"); }
        if ($tag == NULL)  { gotoResultPage("The tag cannot be blank.\n"); }
        
        // Check for appropriate lengths
        if (strlen($title) > $titleSizeLimit) {
            gotoResultPage("Your post title is too big. Please make it 100 characters or less");
        }
        if (strlen($text) > $textSizeLimit) {
            gotoResultPage("Your blog post is too big. Please make it 2000 characters or less");
        }
        if (strlen($tag) > $tagSizeLimit) {
            gotoResultPage("Your blog tag is too big. Please make it 15 characters or less");
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////
        
        $sql = "UPDATE posts SET TITLE='$title', TEXT='$text', TAG='$tag' WHERE ID='$postID'";
        mysqli_query($link, $sql);

        gotoResultPage("The post was updated!");
    }
    // Use this code if delete was pressed
    else if (isset($_POST["Delete"]))
    {
        // Delete post
        $sql = "DELETE FROM posts WHERE ID='$postID'";
        mysqli_query($link, $sql);

        // Delete comments on that post
        $sql = "DELETE FROM comments WHERE POST_ID='$postID'";
        mysqli_query($link, $sql);
        gotoResultPage("The post has been deleted. ");
    }
    else
    {
        gotoResultPage("ERROR: Could not find submit or delete button");
    }
?>