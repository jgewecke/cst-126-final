<?php
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp

require_once('myfuncs.php');

function getNumberOfUsers($link)
{
    $sql = "SELECT ID FROM users";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfPosts($link)
{
    $sql = "SELECT ID FROM posts";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfGuests($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=0";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfModerators($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=1";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

function getNumberOfAdmins($link)
{
    $sql = "SELECT PERMISSION_LEVEL FROM users WHERE PERMISSION_LEVEL=2";
    $result = mysqli_query($link, $sql);
    $numRows = mysqli_num_rows($result);

    return $numRows;
}

?>