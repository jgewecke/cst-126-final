<!-- 
// Project Name: Final Blog
// Project Version: 2.0
// Module Name: Final Blog
// Module Version: 2.0
// Programmer Name: Justin Gewecke
// Date: 8/8/2020
// Description: This is the final version of the project
// References: https://www.w3schools.com/php/php_mysql_insert.asp and https://www.w3schools.com/css/css_website_layout.asp
// https://www.w3schools.com/sql/sql_delete.asp

--NOTE--
Post title is <=100 characters
Post text is <= 2000 characters
-->

<div class="group">
	<a href="post.php">Create another post.</a>
</div>
<div class="group">
	<a href="viewPosts.php">View All Posts.</a>
</div>


<?php
require_once('myfuncs.php');

$link = dbConnect();

// Simple filter
$bannedWords = ['ass', 'fuck', 'bitch', 'cock', 'cum', 'cunt', 'dumbass', 'fag', 'faggot', 'goddamnit', 'goddamn', 'shit'];

$approved = true;
$titleSizeLimit = 100; // Number of characters allowed
$textSizeLimit = 2000; // Number of characters allowed
$tagSizeLimit = 15;    // Number of characters allowed

// Input
$title = $_POST['Title'];
$text = $_POST['TextInput'];
$tag = $_POST['Tag'];
$author = getUserId();

// Check for bad words
foreach ($bannedWords as $badWord) {
    if (strpos(strtolower($text), $badWord) !== false) {
        gotoResultPage("The word '" . $badWord . "' cannot be used. Please remove it and try again.");
    }
    
    if (strpos(strtolower($title), $badWord) !== false) {
        gotoResultPage("The word '" . $badWord . "' cannot be used in your title. Please remove it and try again.");
    }

    if (strpos(strtolower($tag), $badWord) !== false) {
        gotoResultPage("The word '" . $badWord . "' cannot be used in your tag. Please remove it and try again.");
    }
}

// Check for empty fields
if ($text == NULL)  { gotoResultPage("The text field cannot be blank.\n"); }
if ($title == NULL)  { gotoResultPage("The title cannot be blank.\n"); }
if ($tag == NULL)  { gotoResultPage("The tag cannot be blank.\n"); }

// Check for appropriate lengths
if (strlen($title) > $titleSizeLimit) {
    gotoResultPage("Your post title is too big. Please make it 100 characters or less");
}
if (strlen($text) > $textSizeLimit) {
    gotoResultPage("Your blog post is too big. Please make it 2000 characters or less");
}
if (strlen($tag) > $tagSizeLimit) {
    gotoResultPage("Your blog tag is too big. Please make it 15 characters or less");
}


// Attempt insert
$sql = "INSERT INTO posts (AUTHOR_ID, TITLE, TEXT, TAG) VALUES ('$author', '$title', '$text', '$tag')";
if(mysqli_query($link, $sql)){
    gotoResultPage("Your post was submitted.");
} else{
    $message = "ERROR: Could not able to execute $sql. " . mysqli_error($link);
    gotoResultPage('./loginFailed.php');
}
?>